DROP TYPE IF EXISTS DAY_OF_WEEK CASCADE;
CREATE TYPE DAY_OF_WEEK AS ENUM ('MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY');
DROP TYPE IF EXISTS EQUIPMENT_TYPE CASCADE;
CREATE TYPE EQUIPMENT_TYPE AS ENUM ('BEGINNER', 'INTERMEDIATE', 'PROFESSIONAL');
DROP TYPE IF EXISTS LANDSCAPE_TYPE CASCADE;
CREATE TYPE LANDSCAPE_TYPE AS ENUM ('NATURE', 'HUMAN_MADE', 'MIXED');

DROP TABLE IF EXISTS Skiing_base CASCADE;
CREATE TABLE IF NOT EXISTS Skiing_base
(
    id      SERIAL PRIMARY KEY,
    name    VARCHAR(20) NOT NULL UNIQUE CHECK (name != ''),
    region  INTEGER     NOT NULL,
    address VARCHAR(50) NOT NULL CHECK (address != '')
);

DROP TABLE IF EXISTS Information CASCADE;
CREATE TABLE IF NOT EXISTS Information
(
    id          SERIAL PRIMARY KEY,
    skiing_base INTEGER NOT NULL UNIQUE,
    landscape   INTEGER NOT NULL,
    rating      FLOAT   NOT NULL CHECK (rating >= 0 AND rating <= 5)
);

DROP TABLE IF EXISTS Ski_pass CASCADE;
CREATE TABLE IF NOT EXISTS Ski_pass
(
    id          SERIAL PRIMARY KEY,
    skiing_base INTEGER NOT NULL,
    duration    TIME    NOT NULL CHECK (duration > '00:00:00'),
    cost        INTEGER NOT NULL CHECK (cost >= 0)
);

DROP TABLE IF EXISTS Equipment CASCADE;
CREATE TABLE IF NOT EXISTS Equipment
(
    id          SERIAL PRIMARY KEY,
    skiing_base INTEGER        NOT NULL,
    name        VARCHAR(20)    NOT NULL CHECK (name != ''),
    type        EQUIPMENT_TYPE NOT NULL DEFAULT 'INTERMEDIATE',
    cost        INTEGER        NOT NULL CHECK (cost >= 0)
);

DROP TABLE IF EXISTS Landscape CASCADE;
CREATE TABLE IF NOT EXISTS Landscape
(
    id                    SERIAL PRIMARY KEY,
    max_height            INTEGER        NOT NULL CHECK (max_height > 0 AND max_height <= 8848),
    max_slope_angle       FLOAT          NOT NULL CHECK (max_slope_angle > 0 AND max_slope_angle < 90),
    slopes_info           INTEGER        NOT NULL,
    landscape_type        LANDSCAPE_TYPE NOT NULL DEFAULT 'MIXED',
    avalanche_probability FLOAT          NOT NULL CHECK (avalanche_probability >= 0 AND avalanche_probability <= 1),
    last_update           TIMESTAMP      NOT NULL
);

DROP TABLE IF EXISTS Slopes_info CASCADE;
CREATE TABLE IF NOT EXISTS Slopes_info
(
    id        SERIAL PRIMARY KEY,
    green     INTEGER NOT NULL CHECK (green >= 0),
    blue      INTEGER NOT NULL CHECK (blue >= 0),
    red       INTEGER NOT NULL CHECK (red >= 0),
    black     INTEGER NOT NULL CHECK (black >= 0),
    speed_run INTEGER NOT NULL CHECK (speed_run >= 0)
);


DROP TABLE IF EXISTS Region CASCADE;
CREATE TABLE IF NOT EXISTS Region
(
    id   SERIAL PRIMARY KEY,
    name VARCHAR(30) NOT NULL UNIQUE CHECK (name != '')
);


DROP TABLE IF EXISTS Opening_hours CASCADE;
CREATE TABLE IF NOT EXISTS Opening_hours
(
    id          SERIAL PRIMARY KEY,
    skiing_base INTEGER     NOT NULL,
    day         DAY_OF_WEEK NOT NULL,
    opening     TIME        NOT NULL,
    closing     TIME        NOT NULL,
    CHECK (opening <= closing)
);

ALTER TABLE Skiing_base
    ADD CONSTRAINT "skiing_base_fk0" FOREIGN KEY (region) REFERENCES Region (id) ON DELETE CASCADE;
ALTER TABLE Information
    ADD CONSTRAINT "Information_fk0" FOREIGN KEY (skiing_base) REFERENCES Skiing_base (id) ON DELETE CASCADE;
ALTER TABLE Information
    ADD CONSTRAINT "Information_fk1" FOREIGN KEY (landscape) REFERENCES Landscape (id) ON DELETE CASCADE;
ALTER TABLE Ski_pass
    ADD CONSTRAINT "Ski_pass_fk0" FOREIGN KEY (skiing_base) REFERENCES Skiing_base (id) ON DELETE CASCADE;
ALTER TABLE Equipment
    ADD CONSTRAINT "Equipment_fk0" FOREIGN KEY (skiing_base) REFERENCES Skiing_base (id) ON DELETE CASCADE;
ALTER TABLE Landscape
    ADD CONSTRAINT "Landscape_fk0" FOREIGN KEY ("slopes_info") REFERENCES Slopes_info (id) ON DELETE CASCADE;
ALTER TABLE Opening_hours
    ADD CONSTRAINT "Opening_hours_fk0" FOREIGN KEY (skiing_base) REFERENCES Skiing_base (id) ON DELETE CASCADE;


DROP TABLE IF EXISTS Visitor CASCADE;
CREATE TABLE IF NOT EXISTS Visitor
(
    id         SERIAL PRIMARY KEY,
    name       VARCHAR(20) NOT NULL UNIQUE CHECK (name != ''),
    age        INTEGER     NOT NULL CHECK ( age > 0 AND age < 100),
    enter_time TIMESTAMP   NOT NULL
);

DROP TABLE IF EXISTS Visitor_pass CASCADE;
CREATE TABLE IF NOT EXISTS Visitor_pass
(
    id       SERIAL PRIMARY KEY,
    visitor  INTEGER   NOT NULL REFERENCES Visitor (id) ON DELETE CASCADE,
    ski_pass INTEGER   NOT NULL REFERENCES Ski_pass (id) ON DELETE CASCADE,
    buy_time TIMESTAMP NOT NULL
);

DROP TABLE IF EXISTS Base_Equipment CASCADE;
CREATE TABLE IF NOT EXISTS Base_Equipment
(
    id          SERIAL PRIMARY KEY,
    skiing_base INTEGER NOT NULL REFERENCES Skiing_base (id) ON DELETE CASCADE,
    equipment   INTEGER NOT NULL REFERENCES Equipment (id) ON DELETE CASCADE
);

DROP TABLE IF EXISTS Renting CASCADE;
CREATE TABLE IF NOT EXISTS Renting
(
    id             SERIAL PRIMARY KEY,
    visitor        INTEGER   NOT NULL REFERENCES Visitor (id) ON DELETE CASCADE,
    base_equipment INTEGER   NOT NULL REFERENCES Base_Equipment (id) ON DELETE CASCADE,
    buy_time       TIMESTAMP NOT NULL
);



INSERT INTO Base_Equipment (skiing_base, equipment)
SELECT skiing_base, id
FROM Equipment;

ALTER TABLE Equipment
    DROP COLUMN skiing_base;
