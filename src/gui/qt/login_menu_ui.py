# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'login_menu.ui'
#
# Created by: PyQt5 UI code generator 5.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_LoginWindow(object):
    def setupUi(self, LoginWindow):
        LoginWindow.setObjectName("LoginWindow")
        LoginWindow.resize(338, 184)
        self.centralwidget = QtWidgets.QWidget(LoginWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.vertical_layout = QtWidgets.QVBoxLayout()
        self.vertical_layout.setObjectName("vertical_layout")
        self.header_label = QtWidgets.QLabel(self.centralwidget)
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        self.header_label.setFont(font)
        self.header_label.setAlignment(QtCore.Qt.AlignCenter)
        self.header_label.setObjectName("header_label")
        self.vertical_layout.addWidget(self.header_label)
        self.form_layout = QtWidgets.QFormLayout()
        self.form_layout.setHorizontalSpacing(10)
        self.form_layout.setObjectName("form_layout")
        self.login_label = QtWidgets.QLabel(self.centralwidget)
        self.login_label.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.login_label.setObjectName("login_label")
        self.form_layout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.login_label)
        self.login_line_edit = QtWidgets.QLineEdit(self.centralwidget)
        self.login_line_edit.setObjectName("login_line_edit")
        self.form_layout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.login_line_edit)
        self.pswd_label = QtWidgets.QLabel(self.centralwidget)
        self.pswd_label.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.pswd_label.setObjectName("pswd_label")
        self.form_layout.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.pswd_label)
        self.pswd_line_edit = QtWidgets.QLineEdit(self.centralwidget)
        self.pswd_line_edit.setEchoMode(QtWidgets.QLineEdit.Password)
        self.pswd_line_edit.setObjectName("pswd_line_edit")
        self.form_layout.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.pswd_line_edit)
        self.db_label = QtWidgets.QLabel(self.centralwidget)
        self.db_label.setObjectName("db_label")
        self.form_layout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.db_label)
        self.db_line_edit = QtWidgets.QLineEdit(self.centralwidget)
        self.db_line_edit.setObjectName("db_line_edit")
        self.form_layout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.db_line_edit)
        self.schema_label = QtWidgets.QLabel(self.centralwidget)
        self.schema_label.setObjectName("schema_label")
        self.form_layout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.schema_label)
        self.schema_line_edit = QtWidgets.QLineEdit(self.centralwidget)
        self.schema_line_edit.setObjectName("schema_line_edit")
        self.form_layout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.schema_line_edit)
        self.vertical_layout.addLayout(self.form_layout)
        self.connect_btn = QtWidgets.QPushButton(self.centralwidget)
        self.connect_btn.setAutoDefault(True)
        self.connect_btn.setDefault(True)
        self.connect_btn.setObjectName("connect_btn")
        self.vertical_layout.addWidget(self.connect_btn)
        self.gridLayout.addLayout(self.vertical_layout, 0, 0, 1, 1)
        LoginWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(LoginWindow)
        QtCore.QMetaObject.connectSlotsByName(LoginWindow)

    def retranslateUi(self, LoginWindow):
        _translate = QtCore.QCoreApplication.translate
        LoginWindow.setWindowTitle(_translate("LoginWindow", "Login"))
        self.header_label.setText(_translate("LoginWindow", "Database connection"))
        self.login_label.setText(_translate("LoginWindow", "Login"))
        self.pswd_label.setText(_translate("LoginWindow", "Password"))
        self.db_label.setText(_translate("LoginWindow", "Database"))
        self.schema_label.setText(_translate("LoginWindow", "Schemas"))
        self.connect_btn.setText(_translate("LoginWindow", "Connect"))


