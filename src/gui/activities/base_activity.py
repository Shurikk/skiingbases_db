import qdarkstyle
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QMainWindow, QDesktopWidget
from definitions import RESOURCES_DIR


class BaseActivity(QMainWindow):
    def __init__(self, ui_class, parent=None):
        super().__init__(parent=parent)
        self.ui = ui_class()
        self.setup()

    def setup(self):
        self.ui.setupUi(self)
        self.move_to_center()
        self.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())
        self.setWindowIcon(QIcon(RESOURCES_DIR + 'img\\splash_screen.png'))

    def move_to_center(self):
        qt_rectangle = self.frameGeometry()
        center_point = QDesktopWidget().availableGeometry().center()
        qt_rectangle.moveCenter(center_point)
        self.move(qt_rectangle.topLeft())
