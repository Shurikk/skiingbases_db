from PyQt5 import QtGui

from definitions import RESOURCES_DIR
from gui.activities.base_activity import BaseActivity
from gui.qt.diagram_ui import Ui_MainWindow


class DiagramWindow(BaseActivity):
    def __init__(self, parent):
        super(DiagramWindow, self).__init__(Ui_MainWindow, parent)
        self.ui.label.setPixmap(QtGui.QPixmap(RESOURCES_DIR + "img\\db_diagram.png"))
