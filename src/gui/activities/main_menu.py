from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QMessageBox, QTableWidgetItem
from numpy import random as rnd
from psycopg2 import extras, Error as dbError
from pandas.io.sql import DatabaseError as pandasDBError

from analyzer.analyzer import Analyzer
from db_helper import get_table_attributes, clear_db, get_tables_full_names
from generator.SkiingBasesGenerator import Generator
from gui.activities.base_activity import BaseActivity
from gui.activities.diagram_window import DiagramWindow
from gui.qt.main_menu_ui import Ui_MainWindow
from definitions import RESOURCES_DIR
from db_helper import get_last_id

# expected structure
tables = ['region', 'skiing_base', 'slopes_info', 'landscape',
          'information', 'ski_pass', 'equipment', 'base_equipment',
          'opening_hours', 'visitor', 'visitor_pass', 'renting']


class MainMenuWindow(BaseActivity):
    def __init__(self, conn, schemas, parent=None):
        super(MainMenuWindow, self).__init__(Ui_MainWindow, parent)
        self.statusBar().showMessage('Loading..')
        # connection
        self.conn = conn
        self.conn.autocommit = True
        self.cursor = conn.cursor()
        self.schemas = '\"{}\"'.format(schemas)
        self.check_db_structure()

        # states
        self.gen_current_table = tables[0]
        self.ins_current_table = tables[0]
        self.view_current_table = tables[0]
        self.del_current_table = tables[0]
        self.analyzer_current_table = tables[0]
        self.current_rows_count = 1
        self.current_view_rows_count = 1

        # ui settings
        self._main_ui_setup()
        self._gen_ui_setup()
        self._ins_ui_setup()
        self._view_ui_setup()
        self._del_ui_setup()
        self._analyzer_ui_setup()

        # modules init
        self.generator = Generator(self.conn, self.cursor, self.schemas)
        self.analyzer = Analyzer(self.conn, self.schemas)

        self.statusBar().showMessage('Ready')

    def _main_ui_setup(self):
        self.ui.show_button.clicked.connect(self.show_db_diagram)
        self.ui.create_button.clicked.connect(self.create_db)
        self.ui.clear_button.clicked.connect(self.clear_db)
        self.ui.destroy_button.clicked.connect(self.destroy_db)

    def _gen_ui_setup(self):
        self.ui.tables_combo_box.addItems(tables)
        self.ui.tables_combo_box.activated.connect(self._gen_table_combo_handle)
        self.ui.gen_button.clicked.connect(self.generate)
        self.ui.gen_all_button.clicked.connect(self.generate_all)
        self.ui.rnd_check.stateChanged.connect(self._rnd_check_handler)
        self.ui.rnd_check.setCheckState(Qt.Unchecked)
        self._rnd_check_handler(Qt.Unchecked)

    def _ins_ui_setup(self):
        self.ui.insert_combo_box.addItems(tables)
        self.ui.insert_combo_box.activated.connect(self._ins_table_combo_handle)
        self.ui.rows_spin_box.setValue(self.current_rows_count)
        self._ins_rows_count_handler()
        self._ins_table_fields_upd()
        self.ui.insert_button.clicked.connect(self.insert)
        self.ui.rows_spin_box.valueChanged.connect(self._ins_rows_count_handler)

    def _view_ui_setup(self):
        self.ui.view_combo_box.addItems(tables)
        self.ui.view_combo_box.activated.connect(self._view_table_combo_handle)
        self.ui.limit_spin_box.setValue(self.current_view_rows_count)
        self._view_rows_count_handler()
        self._view_table_fields_upd()
        self.ui.view_button.clicked.connect(self.display)
        self.ui.limit_spin_box.valueChanged.connect(self._view_rows_count_handler)

    def _del_ui_setup(self):
        self.ui.del_combo_box.addItems(tables)
        self.ui.del_combo_box.activated.connect(self._del_table_combo_handle)
        self.ui.del_button.clicked.connect(self.del_data)
        self.ui.cond_combo_box.addItems(['=', '!=', '>', '>=', '<', '<='])
        self._del_operands_fill()

    def _analyzer_ui_setup(self):
        self.ui.analyze_table_combo_box.addItems(tables)
        self.ui.analyze_table_combo_box.activated.connect(self._analyze_table_combo_handle)
        self._analyzer_table_fields_upd()
        self.ui.analyze_button.clicked.connect(self.analyze)

    def _gen_table_combo_handle(self, index):
        self.gen_current_table = self.ui.tables_combo_box.itemText(index)

    def _ins_table_combo_handle(self, index):
        self.ins_current_table = self.ui.insert_combo_box.itemText(index)
        self._ins_table_fields_upd()

    def _view_table_combo_handle(self, index):
        self.view_current_table = self.ui.view_combo_box.itemText(index)
        self._view_table_fields_upd()

    def _del_table_combo_handle(self, index):
        self.del_current_table = self.ui.del_combo_box.itemText(index)
        self._del_operands_fill()

    def _analyze_table_combo_handle(self, index):
        self.analyzer_current_table = self.ui.analyze_table_combo_box.itemText(index)
        self._analyzer_table_fields_upd()

    def _ins_table_fields_upd(self):
        attr = get_table_attributes(self.cursor, self.schemas, self.ins_current_table)
        self.ui.insert_table.setColumnCount(len(attr) - 1)
        self.ui.insert_table.setHorizontalHeaderLabels(attr[1:])

    def _ins_rows_count_handler(self):
        self.current_rows_count = self.ui.rows_spin_box.value()
        self.ui.insert_table.setRowCount(self.current_rows_count)

    def _view_table_fields_upd(self):
        attr = get_table_attributes(self.cursor, self.schemas, self.view_current_table)
        self.ui.view_table.setColumnCount(len(attr))
        self.ui.view_table.setHorizontalHeaderLabels(attr)
        self._clear_view_table()

    def _view_rows_count_handler(self):
        self.current_view_rows_count = self.ui.limit_spin_box.value()
        self.ui.view_table.setRowCount(self.current_view_rows_count)

    def _clear_view_table(self):
        self.ui.view_table.setRowCount(0)
        self.ui.view_table.setRowCount(self.current_view_rows_count)

    def _del_operands_fill(self):
        attr = get_table_attributes(self.cursor, self.schemas, self.del_current_table)
        self.ui.operand_combo_box.clear()
        self.ui.operand_combo_box.addItems(attr)

    def _analyzer_table_fields_upd(self):
        attr = 'all', *get_table_attributes(self.cursor, self.schemas, self.analyzer_current_table)[1:]
        self.ui.analyze_attr_combo_box.clear()
        self.ui.analyze_attr_combo_box.addItems(attr)

    def _rnd_check_handler(self, state):
        is_random = state == Qt.Checked
        # random settings
        self.ui.from_spin.setEnabled(is_random)
        self.ui.from_label.setEnabled(is_random)
        self.ui.to_spin.setEnabled(is_random)
        self.ui.to_label.setEnabled(is_random)
        self.ui.rnd_bounds_label.setEnabled(is_random)
        # fixed settings
        self.ui.amount_label.setEnabled(not is_random)
        self.ui.sb_spin.setEnabled(not is_random)

    def closeEvent(self, event):
        close = QMessageBox.question(self, "QUIT", "Do you really want to stop process?",
                                     QMessageBox.Yes | QMessageBox.No)
        if close == QMessageBox.Yes:
            self._close_action()
            event.accept()
        else:
            event.ignore()

    def _close_action(self):
        self.conn.close()

    def check_db_structure(self):
        try:
            db_tables = [db_t.split('.')[1] for db_t in get_tables_full_names(self.cursor, self.schemas)]
            if all([t in db_tables for t in tables]) and len(db_tables) == len(tables):
                return True
        except dbError:
            pass
        print('DB structure problems. Recreating DB')
        return self.create_db(force=True)

    def db_execute(self, sql):
        try:
            self.cursor.execute(sql)
            return True
        except dbError as e:
            self.statusBar().showMessage('Interrupted')
            QMessageBox.information(self, 'DB error', "Error description:\n" + e.pgerror)
            return False

    def _get_amount(self):
        is_random = self.ui.rnd_check.isChecked()
        if is_random:
            bounds = self.ui.from_spin.value(), self.ui.to_spin.value()
            if bounds[0] >= bounds[1]:
                return
            amount = rnd.randint(self.ui.from_spin.value(), self.ui.to_spin.value())
        else:
            amount = self.ui.sb_spin.value()
        return amount

    def __base_generate(self, is_all=False):
        self.statusBar().showMessage('Generating data..')
        amount = self._get_amount()
        if amount is None:
            self.statusBar().showMessage('Interrupted')
            QMessageBox.information(self, 'Warning', "'From' bound must be lesser than 'to' bound!")
            self.statusBar().showMessage('Ready')
            return
        try:
            if is_all:
                self.generator.insert_full_data(amount)
                self.statusBar().showMessage('Completed')
                real_count = amount * len(tables)
                samples = 'sample' + ('s' if real_count > 1 else '')
                QMessageBox.information(self, 'Generation info',
                                        'Successfully added {0} new data {1} to all tables'.format(real_count, samples))
            else:
                self.generator.insert_table(self.gen_current_table, amount)
                self.statusBar().showMessage('Completed')
                samples = 'sample' + ('s' if amount > 1 else '')
                QMessageBox.information(self, 'Generation info',
                                        'Successfully added {0} new data {1} to \'{2}\' table'.format(
                                            amount, samples, self.gen_current_table))
        except dbError as e:
            self.statusBar().showMessage('Interrupted')
            QMessageBox.information(self, 'DB error', "Error description:\n" + e.pgerror)
        self.statusBar().showMessage('Ready')

    def generate(self):
        self.__base_generate()

    def generate_all(self):
        self.__base_generate(is_all=True)

    def get_insertion_data(self):
        ins_index = get_last_id(self.cursor, self.schemas, self.ins_current_table) + 1
        rows = self.ui.insert_table.rowCount()
        cols = self.ui.insert_table.columnCount()
        data = []
        for row in range(rows):
            tmp = [ins_index]
            for col in range(cols):
                try:
                    tmp.append(self.ui.insert_table.item(row, col).text())
                    if tmp[-1] == '':
                        break
                except AttributeError:
                    break
            else:
                data.append(tuple(tmp))
                ins_index += 1
        return data

    def insert(self):
        try:
            data = self.get_insertion_data()
            if len(data) == 0:
                self.statusBar().showMessage('Interrupted')
                QMessageBox.information(self, 'Insertion warning',
                                        "There's no completed row. You have to specify at least one row.")
                self.statusBar().showMessage('Ready')
                return
            self.statusBar().showMessage('Inserting data..')
            insert_query = 'INSERT INTO {} VALUES %s'.format(self.schemas + '.' + self.ins_current_table)
            extras.execute_values(self.cursor, insert_query, data, template=None, page_size=100)
        except dbError as e:
            self.statusBar().showMessage('Interrupted')
            QMessageBox.information(self, 'Insertion error', "Error description:\n" + e.pgerror)
        else:
            self.statusBar().showMessage('Completed')
            data_len = len(data)
            samples = 'sample' + ('s' if data_len > 1 else '')
            QMessageBox.information(
                self, 'Insertion result', 'Successfully added {0} new data {1} to \'{2}\' table'.format(
                    data_len, samples, self.ins_current_table))
        self.statusBar().showMessage('Ready')

    def _fill_view_table(self, rows):
        self._clear_view_table()
        for i in range(len(rows)):
            for j in range(len(rows[0])):
                self.ui.view_table.setItem(i, j, QTableWidgetItem(str(rows[i][j])))

    def display(self):
        self.statusBar().showMessage('Displaying data..')
        sql = 'SELECT * FROM {} LIMIT {};'.format(self.schemas + '.' + self.view_current_table,
                                                  self.ui.limit_spin_box.value())
        try:
            self.cursor.execute(sql)
            res_rows = self.cursor.fetchall()
            self.statusBar().showMessage('Completed')
            # QMessageBox.information(self, 'Displaying', "Data has been successfully displayed.")
        except dbError as e:
            self.statusBar().showMessage('Interrupted')
            QMessageBox.information(self, 'DB error', "Error description:\n" + e.pgerror)
        else:
            self._fill_view_table(res_rows)
        self.statusBar().showMessage('Ready')

    def show_db_diagram(self):
        diagram_window = DiagramWindow(parent=self)
        diagram_window.show()

    def create_db(self, force=False):
        self.statusBar().showMessage('Creating database..')
        decision = QMessageBox.Yes if force else QMessageBox.question(self, "Warning",
                                                                      "Do you really want to recreate DB structure?\n"
                                                                      "This will clear all your data!",
                                                                      QMessageBox.Yes | QMessageBox.No)
        if decision == QMessageBox.Yes:
            res = self.db_execute(open(RESOURCES_DIR + 'data\\Creation.sql', 'r').read())
            log_msg = 'Completed' if res else 'Ready'
            self.statusBar().showMessage(log_msg)
            return True
        else:
            self.statusBar().showMessage('Ready')
            return False

    def clear_db(self):
        self.statusBar().showMessage('Clearing database..')
        decision = QMessageBox.question(self, "Warning", "Do you really want to clear DB?\n"
                                                         "This will clear all your data!",
                                        QMessageBox.Yes | QMessageBox.No)
        if decision == QMessageBox.Yes:
            try:
                clear_db(self.cursor, self.schemas)
            except dbError as e:
                QMessageBox.information(self, 'DB error', "Error description:\n" + e.pgerror)
            else:
                self.statusBar().showMessage('Completed')
        else:
            self.statusBar().showMessage('Ready')

    def destroy_db(self):
        self.statusBar().showMessage('Dropping database schemas..')
        decision = QMessageBox.question(self, "Warning", "Do you really want to drop DB?\n"
                                                         "This will clear all your data and DB structure!",
                                        QMessageBox.Yes | QMessageBox.No)
        if decision == QMessageBox.Yes:
            sql = r'DROP SCHEMA {} CASCADE;' \
                  r'CREATE SCHEMA {};'.format(self.schemas, self.schemas)
            res = self.db_execute(sql)
            log_msg = 'Warning! No DB structure!' if res else 'Ready'
            self.statusBar().showMessage(log_msg)
        else:
            self.statusBar().showMessage('Ready')

    def del_data(self):
        self.statusBar().showMessage('Deleting data..')
        sql = 'DELETE FROM {} WHERE {} {} {};'.format(self.schemas + '.' + self.del_current_table,
                                                      self.ui.operand_combo_box.currentText(),
                                                      self.ui.cond_combo_box.currentText(),
                                                      self.ui.cond_line_edit.text())
        res = self.db_execute(sql)
        if res:
            self.statusBar().showMessage('Completed')
            QMessageBox.information(self, 'Deletion', "Data has been successfully deleted by your condition.")
        self.statusBar().showMessage('Ready')

    def analyze(self):
        self.statusBar().showMessage('Analyzing data..')
        attr = self.ui.analyze_attr_combo_box.currentText()
        columns = None if attr == 'all' else attr
        try:
            if self.ui.stats_button.isChecked():
                stats = self.analyzer.describe_table(self.analyzer_current_table, column=columns)
                self.statusBar().showMessage('Completed')
                QMessageBox.information(self, 'Statistics',
                                        'Statistics of {} attribute(s) from {} table:\n\n{}'
                                        .format(attr, self.analyzer_current_table, stats))
            elif self.ui.plot_button.isChecked():
                if columns is not None:
                    self.analyzer.table_plot(self.analyzer_current_table, column=columns)
                else:
                    self.statusBar().showMessage('Interrupted')
                    QMessageBox.information(self, 'Plotting warning',
                                            "You have to specify a single attribute for linear plot")
            elif self.ui.barplot_button.isChecked():
                self.analyzer.table_hist_plot(self.analyzer_current_table, column=columns)
            elif self.ui.boxplot_Button.isChecked():
                self.analyzer.table_box_plot(self.analyzer_current_table, column=columns)
            else:
                self.statusBar().showMessage('Interrupted')
                QMessageBox.information(self, 'Analyzing warning', "Some analyzer settings are uncompleted")
        except dbError as e:
            self.statusBar().showMessage('Interrupted')
            QMessageBox.information(self, 'DB error', "Error description:\n" + e.pgerror)
        except pandasDBError:
            self.statusBar().showMessage('Interrupted')
            QMessageBox.information(self, 'DB error', "DB structure error!\n")
        except (ValueError, TypeError):
            self.statusBar().showMessage('Interrupted')
            QMessageBox.information(self, 'Plotting warning',
                                    "You have to specify a numeric attribute for plot")
        self.statusBar().showMessage('Ready')
