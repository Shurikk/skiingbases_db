import psycopg2
from PyQt5.QtWidgets import QShortcut, QMessageBox

from gui.activities.base_activity import BaseActivity
from generator.HelpFunctions import print_separator
from gui.qt.login_menu_ui import Ui_LoginWindow
from gui.activities.main_menu import MainMenuWindow


class LoginMenuWindow(BaseActivity):
    def __init__(self):
        super(LoginMenuWindow, self).__init__(Ui_LoginWindow)
        self.ui.connect_btn.clicked.connect(self.establish_connection)
        self.login_attempts = 3

        self.shortcut = QShortcut("Enter", self)
        self.shortcut.activated.connect(self.establish_connection)

        # defaults
        self.ui.db_line_edit.setText('postgres')
        self.ui.schema_line_edit.setText('public')
        self.ui.login_line_edit.setText('postgres')

    def establish_connection(self):
        try:
            user_data = self.ui.login_line_edit.text()
            pswd_data = self.ui.pswd_line_edit.text()
            conn = psycopg2.connect(dbname='postgres', user=user_data, password=pswd_data, host='localhost')
        except psycopg2.OperationalError:
            self.login_attempts -= 1
            QMessageBox.information(self, 'Login error',
                                    "Invalid login or password!\n{} attempts left".format(self.login_attempts))
            print('Invalid login or password')
            print('({} attempts left)'.format(self.login_attempts))
            print_separator()
            if self.login_attempts == 0:
                self.close_program()
        except psycopg2.DatabaseError as db_error:
            QMessageBox.information(self, 'Login error', "Unexpected DB error.")
            print('Unexpected DB error.')
            print(db_error.pgerror)
            self.close_program()
        else:
            self.connect(conn)
            print('Successfully connected to SkiingBases DB')

    def connect(self, conn):
        self.close()
        main_menu = MainMenuWindow(parent=self,
                                   schemas=self.ui.schema_line_edit.text(),
                                   conn=conn)
        main_menu.show()

    def close_program(self):
        self.close()
