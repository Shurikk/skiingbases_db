import pandas.io.sql as sqlio
from matplotlib import pyplot as plt


class Analyzer:
    def __init__(self, conn, schemas):
        self.conn = conn
        self.schemas = schemas

    def describe_table(self, table, column=None):
        df_selected = self.__get_converted_df(table, column)
        return df_selected.describe()

    def table_plot(self, table, column=None):
        df_selected = self.__get_converted_df(table, column)
        df_selected.plot()
        plt.show()

    def table_hist_plot(self, table, column=None):
        df_selected = self.__get_converted_df(table, column)
        df_selected.hist()
        plt.show()

    def table_box_plot(self, table, column=None):
        df_selected = self.__get_converted_df(table, column)
        df_selected.boxplot()
        plt.show()

    def __table_to_df(self, table):
        sql = "SELECT * from {};".format(self.schemas + '.' + table)
        df = sqlio.read_sql_query(sql, self.conn)
        df.drop('id', axis=1, inplace=True)
        return df

    def __get_converted_df(self, table, column=None):
        df = self.__table_to_df(table)
        df_selected = df if column is None else df[[column]]
        return df_selected
