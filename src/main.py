import sys
from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication
from PyQt5 import QtGui, QtWidgets

from gui.activities.login_menu import LoginMenuWindow
from definitions import RESOURCES_DIR


def launch_splash_screen():
    global splash
    splash_pix = QtGui.QPixmap(RESOURCES_DIR + 'img\\splash_screen.png')
    splash = QtWidgets.QSplashScreen(splash_pix, QtCore.Qt.WindowStaysOnTopHint)
    splash.show()


def launch_app():
    splash.close()
    global main_activity
    main_activity = LoginMenuWindow()
    main_activity.show()


def main():
    app = QApplication(sys.argv)
    launch_splash_screen()
    QtCore.QTimer.singleShot(1000, launch_app)
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
