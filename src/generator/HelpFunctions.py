from timeit import default_timer as timer
from functools import wraps

powers = {
    0: 'sec',
    3: 'ms',
    6: 'us',
    9: 'ns',
    12: 'ps'
}


def print_separator():
    print("=" * 70)


def question(text=''):
    return str(input(text + " (y/n)? ")).lower() == 'y'


def str_2_bool(v):
    return v.lower() in ("yes", "true", "t", "1")


# decorator
def benchmark(iterations=1, power=0, total_time=None):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            global f_res
            grade = pow(10, power)
            units = powers[power]
            average_time = 0.0
            for i in range(0, iterations):
                t0 = timer()
                f_res = func(*args, **kwargs)
                t1 = timer()
                average_time += (t1 - t0) * grade
            average_time /= float(iterations)
            if total_time is not None:
                total_time[0] += average_time

            print('Operation has taken\t%f %s\t' % (average_time, units))
            print_separator()
            return f_res

        return wrapper

    return decorator
