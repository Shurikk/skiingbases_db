import string
import random
from datetime import datetime, timedelta
from numpy import random as rnd

MAX_VALUE = pow(2, 31) - 1


def name_gen(max_l, min_l=4):
    name_len = rnd.randint(min_l, max_l)
    name = ''.join(random.choices(string.ascii_uppercase, k=1))
    name += ''.join(random.choices(string.ascii_lowercase, k=name_len - 1))
    if name_len < max_l - min_l - 1 and rnd.randint(0, 2):
        name += ' ' + name_gen(max_l - name_len - 1)
    return name


def name_list_gen(count, max_l, min_l=4, unique=False, unique_list=None, attempts=30):
    if unique_list is None:
        unique_list = []
    names = []
    i = 0
    remaining = attempts
    while i < count:
        name = name_gen(max_l, min_l)
        if not unique or (name not in names and name not in unique_list):
            names.append(name)
            i += 1
            remaining = attempts
        elif remaining == 0:
            i += 1
            remaining = attempts
        else:
            i -= 1
            remaining -= 1
    return names


def int_list_gen(count, min_value=0, max_value=int(MAX_VALUE), unique=False, unique_list=None, attempts=25):
    if unique_list is None:
        unique_list = []
    lst = []
    i = 0
    remaining = attempts
    while i < count:
        value = rnd.randint(min_value, max_value)
        if not unique or (value not in lst and value not in unique_list):
            lst.append(value)
            i += 1
            remaining = attempts
        elif remaining == 0:
            i += 1
            remaining = attempts
        else:
            i -= 1
            remaining -= 1

    return lst


def float_list_gen(count, min_value=0, max_value=float(MAX_VALUE)):
    return rnd.uniform(min_value, max_value, count)


def datetime_gen(date=True, timing=True, start=datetime(1970, 1, 1), end=datetime.today()):
    data = start + (end - start) * random.random()
    res = ''
    if date:
        res += data.strftime("%Y-%m-%d")
    if date and timing:
        res += ' '
    if timing:
        res += data.strftime("%H:%M:%S")
    return res


def datetime_list_gen(count, date=True, timing=True, allow_zero=True,
                      start=datetime(1970, 1, 1), end=datetime.today(), delta_years=0, delta_months=0, delta_days=0):
    lst = []
    real_start = end - timedelta(days=int(delta_years * 365.25) + delta_months * 30 + delta_days) \
        if delta_months + delta_years + delta_days != 0 else start
    for i in range(0, count):
        time_data = datetime_gen(date, timing, real_start, end)
        if not allow_zero and time_data == '00:00:00':
            time_data = '00:00:01'
        lst.append(time_data)
    return lst


def timedelta_list_gen(count):
    lst = []
    max_border = datetime.strptime("23:59:50", "%H:%M:%S")
    for i in range(0, count):
        t0 = datetime_gen(date=False, timing=True, end=max_border)
        start = datetime.strptime(t0, "%H:%M:%S") + timedelta(seconds=1)
        end = datetime.strptime("23:59:59", "%H:%M:%S")
        t1 = datetime_gen(date=False, timing=True, start=start, end=end)
        lst.append([t0, t1])
    return lst


def rnd_list_elements_gen(count, lst, unique=True):
    if unique:
        return random.sample(lst, min(count, len(lst)))
    else:
        return random.choices(lst, k=count)


def rnd_restricted_list_gen(count, lst, restricted):
    res = []
    max_attempts = 15
    for i in range(min(count, len(restricted))):
        attempts = max_attempts
        while attempts > 0:
            element = lst[rnd.randint(0, len(lst))]
            if element != restricted[i]:
                res.append(element)
                break
            attempts -= 1
    return res
