import configparser

from definitions import RESOURCES_DIR
from generator.HelpFunctions import str_2_bool

global_encoding = 'utf-8-sig'


def load_section_from_config(config_name, config_section):
    config = configparser.ConfigParser()
    config.read(RESOURCES_DIR + 'data\\' + config_name, encoding=global_encoding)
    return dict(config[config_section])


def load_settings(config_name, config_section='settings'):
    data = load_section_from_config(config_name, config_section)
    res_data = {key: str_2_bool(data[key]) for key in data.keys()}
    res_data['login_attempts'] = int(data['login_attempts'])
    return res_data


def load_coefficients(config_name, config_section='coefficients'):
    data = load_section_from_config(config_name, config_section)
    data_int = {key: int(data[key]) for key in data.keys()}
    return data_int
