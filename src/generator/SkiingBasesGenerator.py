from psycopg2 import extras
from cachetools import cached, LFUCache
from functools import wraps

from db_helper import get_last_id, get_rows_from_table, cursor_res_to_list, clear_db
from generator.DataGenerator import name_list_gen, int_list_gen, float_list_gen, datetime_list_gen, \
    timedelta_list_gen, rnd_list_elements_gen, rnd_restricted_list_gen

# data
from generator.HelpFunctions import benchmark

cache_size = 10
total_time = [0.0]


def data_insert(func):
    @benchmark(total_time=total_time)
    @wraps(func)
    def wrapper(self, count, *args, **kwargs):
        table_name = func.__name__.replace('insert_', '')
        real_count = count
        if self.with_param:
            real_count *= self.parameters[table_name]
        if real_count == 0:
            print('No new data has been added to \'{}\' table'.format(table_name))
            return

        res = func(self, real_count, table_name, *args, **kwargs)
        self.insertion(table_name, res)
        samples = 'sample' + ('s' if count > 1 else '')
        print('Successfully added {0} new data {1} to \'{2}\' table'.format(real_count, samples, table_name))
        return res

    return wrapper


class Generator:
    # system settings
    param_config_name = 'parameters.ini'
    page_gradation = {'TINY': 500, 'SMALL': 1000, 'AVERAGE': 5000, 'HUGE': 10000}

    def __init__(self, conn, cursor, schemas):
        self.conn = conn
        self.cursor = cursor
        self.schemas = schemas

        self.with_param = False

        # self.parameters = load_coefficients(Generator.param_config_name)
        self.parameters = None

        self.enums = {'DAY_OF_WEEK': [], 'EQUIPMENT_TYPE': [], 'LANDSCAPE_TYPE': []}
        self.preload_types()

    def insertion(self, table, data):
        insert_query = 'INSERT INTO {} VALUES %s'.format('{}.'.format(self.schemas) + table)
        extras.execute_values(self.cursor, insert_query, data, template=None,
                              page_size=Generator.page_gradation['AVERAGE'])

    def convert_data(self, start_id, *args):
        data_lst = []
        count = min([len(arg) for arg in args])
        ids = [i for i in range(start_id, start_id + count)]
        for i in range(count):
            element = [ids[i]]
            element.extend([arg[i] for arg in args])
            data_lst.append(tuple(element))
        return data_lst

    @cached(LFUCache(maxsize=cache_size))
    def get_ids_from_table(self, table):
        return get_rows_from_table(self.cursor, self.schemas, table, 'id')

    def get_names_from_table(self, table):
        return get_rows_from_table(self.cursor, self.schemas, table, 'name')

    def get_rnd_fk_from_table(self, count, table, unique=False):
        return rnd_list_elements_gen(count=count, lst=self.get_ids_from_table(table), unique=unique)

    def load_enum(self, enum_name):
        self.cursor.execute('SELECT enum_range(NULL::{});'.format('{}.'.format(self.schemas) + enum_name))
        return cursor_res_to_list(self.cursor)[0][1:-1].split(',')

    @data_insert
    def insert_skiing_base(self, count, table_name=''):
        base_last_id = get_last_id(self.cursor, self.schemas, table_name) + 1
        names = name_list_gen(count=count, max_l=20, unique=True, unique_list=self.get_names_from_table(table_name))
        region = self.get_rnd_fk_from_table(count=count, table='region')
        addresses = name_list_gen(count=count, max_l=50)
        return self.convert_data(base_last_id, *[names, region, addresses])

    @data_insert
    def insert_information(self, count, table_name=''):
        info_last_id = get_last_id(self.cursor, self.schemas, table_name) + 1
        self.cursor.execute(
            'SELECT id FROM {}.skiing_base '
            'WHERE id NOT IN (SELECT {}.information.skiing_base FROM {}.information);'.format(self.schemas,
                                                                                              self.schemas,
                                                                                              self.schemas))
        allowed_names = cursor_res_to_list(self.cursor)
        bases = rnd_list_elements_gen(count=count, lst=allowed_names)
        landscapes = self.get_rnd_fk_from_table(count=count, table='landscape')
        ratings = float_list_gen(count=count, min_value=0, max_value=5)
        return self.convert_data(info_last_id, *[bases, landscapes, ratings])

    @data_insert
    def insert_ski_pass(self, count, table_name=''):
        pass_last_id = get_last_id(self.cursor, self.schemas, table_name) + 1
        bases = self.get_rnd_fk_from_table(count=count, table='skiing_base')
        durations = datetime_list_gen(count=count, date=False, timing=True, allow_zero=False)
        costs = int_list_gen(count=count)
        return self.convert_data(pass_last_id, *[bases, durations, costs])

    @data_insert
    def insert_equipment(self, count, table_name=''):
        equipment_last_id = get_last_id(self.cursor, self.schemas, table_name) + 1
        names = name_list_gen(count=count, max_l=20)
        types = rnd_list_elements_gen(count=count, lst=self.enums['EQUIPMENT_TYPE'], unique=False)
        costs = int_list_gen(count=count, max_value=1000000)
        return self.convert_data(equipment_last_id, *[names, types, costs])

    @data_insert
    def insert_base_equipment(self, count, table_name=''):
        base_equipment_last_id = get_last_id(self.cursor, self.schemas, table_name) + 1
        bases = self.get_rnd_fk_from_table(count=count, table='skiing_base')
        equipments = self.get_rnd_fk_from_table(count=count, table='equipment')
        return self.convert_data(base_equipment_last_id, *[bases, equipments])

    @data_insert
    def insert_slopes_info(self, count, table_name=''):
        slopes_last_id = get_last_id(self.cursor, self.schemas, table_name) + 1
        max_slopes = 5000
        green_slopes = int_list_gen(count=count, max_value=max_slopes)
        blue_slopes = int_list_gen(count=count, max_value=max_slopes)
        red_slopes = int_list_gen(count=count, max_value=max_slopes)
        black_slopes = int_list_gen(count=count, max_value=max_slopes)
        speed_slopes = int_list_gen(count=count, max_value=max_slopes)
        return self.convert_data(slopes_last_id, *[green_slopes, blue_slopes, red_slopes, black_slopes, speed_slopes])

    @data_insert
    def insert_landscape(self, count, table_name=''):
        landscape_last_id = get_last_id(self.cursor, self.schemas, table_name) + 1
        heights = int_list_gen(count=count, min_value=1, max_value=8849)
        angles = float_list_gen(count=count, min_value=1, max_value=90)
        slopes = self.get_rnd_fk_from_table(count=count, table='slopes_info')
        types = rnd_list_elements_gen(count=count, lst=self.enums['LANDSCAPE_TYPE'], unique=False)
        probabilities = float_list_gen(count=count, min_value=0, max_value=1)
        dates = datetime_list_gen(count=count, date=True, timing=True, delta_months=5)
        return self.convert_data(landscape_last_id, *[heights, angles, slopes, types, probabilities, dates])

    @data_insert
    def insert_region(self, count, table_name=''):
        info_last_id = get_last_id(self.cursor, self.schemas, table_name) + 1
        restricted_names = self.get_names_from_table(table_name)
        names = name_list_gen(count=count, max_l=30, unique=True, unique_list=restricted_names)
        return self.convert_data(info_last_id, *[names])

    @data_insert
    def insert_opening_hours(self, count, table_name=''):
        opening_last_id = get_last_id(self.cursor, self.schemas, table_name) + 1
        bases = self.get_rnd_fk_from_table(count=count, table='skiing_base', unique=True)
        self.cursor.execute(
            'SELECT day FROM {}.opening_hours '
            'WHERE skiing_base IN ({});'.format(self.schemas, str(bases)[1:-1]))
        restricted_days = cursor_res_to_list(self.cursor)
        if len(restricted_days) != 0:
            days = rnd_restricted_list_gen(count=count, lst=self.enums['DAY_OF_WEEK'], restricted=restricted_days)
        else:
            days = self.enums['DAY_OF_WEEK'] * len(bases)
            bases = bases * len(self.enums['DAY_OF_WEEK'])
        count = len(days)
        intervals = timedelta_list_gen(count=count)
        return self.convert_data(opening_last_id,
                                 *[bases[:min(count, len(bases))], days, [t[0] for t in intervals],
                                   [t[1] for t in intervals]])

    @data_insert
    def insert_visitor(self, count, table_name=''):
        visitors_last_id = get_last_id(self.cursor, self.schemas, table_name) + 1
        names = name_list_gen(count=count, max_l=20, unique=True)
        ages = int_list_gen(count=count, min_value=1, max_value=100)
        timings = datetime_list_gen(count=count, date=True, timing=True, delta_months=6)
        return self.convert_data(visitors_last_id, *[names, ages, timings])

    @data_insert
    def insert_visitor_pass(self, count, table_name=''):
        visitor_pass_last_id = get_last_id(self.cursor, self.schemas, table_name) + 1
        visitors = self.get_rnd_fk_from_table(count=count, table='visitor')
        passes = self.get_rnd_fk_from_table(count=count, table='ski_pass')
        timings = datetime_list_gen(count=count, date=True, timing=True, delta_months=3)
        return self.convert_data(visitor_pass_last_id, *[visitors, passes, timings])

    @data_insert
    def insert_renting(self, count, table_name=''):
        renting_last_id = get_last_id(self.cursor, self.schemas, table_name) + 1
        visitors = self.get_rnd_fk_from_table(count=count, table='visitor')
        equipments = self.get_rnd_fk_from_table(count=count, table='base_Equipment')
        timings = datetime_list_gen(count=count, date=True, timing=True, delta_months=3)
        return self.convert_data(renting_last_id, *[visitors, equipments, timings])

    def insert_full_data(self, count):
        clear_db(self.cursor, self.schemas)
        self.insert_region(count)
        self.insert_skiing_base(count)
        self.insert_slopes_info(count)
        self.insert_landscape(count)
        self.insert_information(count)
        self.insert_ski_pass(count)
        self.insert_equipment(count)
        self.insert_base_equipment(count)
        self.insert_opening_hours(count)
        self.insert_visitor(count)
        self.insert_visitor_pass(count)
        self.insert_renting(count)

    def insert_table(self, table, count):
        table_data_gen = {
            'region': self.insert_region,
            'skiing_base': self.insert_skiing_base,
            'slopes_info': self.insert_slopes_info,
            'landscape': self.insert_landscape,
            'information': self.insert_information,
            'ski_pass': self.insert_ski_pass,
            'equipment': self.insert_equipment,
            'base_equipment': self.insert_base_equipment,
            'opening_hours': self.insert_opening_hours,
            'visitor': self.insert_visitor,
            'visitor_pass': self.insert_visitor_pass,
            'renting': self.insert_renting
        }
        table_data_gen[table](count)

    def preload_types(self):
        for key in self.enums:
            self.enums[key] = self.load_enum(key)
