import json


def cursor_res_to_list(cursor):
    return [row[0] for row in cursor.fetchall()]


def get_tables_full_names(cursor, schemas):
    # noinspection SqlResolve
    cursor.execute(
        'SELECT table_name FROM information_schema.tables '
        'WHERE table_schema=\'{}\' AND table_type=\'BASE TABLE\';'.format(schemas[1:-1]))
    return ['{}.'.format(schemas) + element[0] for element in cursor.fetchall()]


def clear_db(cursor, schemas):
    t_names = ", ".join(get_tables_full_names(cursor, schemas))
    cursor.execute('TRUNCATE {} CASCADE'.format(t_names))


def get_last_id(cursor, schemas, table):
    # noinspection SqlResolve
    cursor.execute('SELECT Max(id) FROM {};'.format('{}.'.format(schemas) + table))
    res = cursor.fetchone()[0]
    return res or 0


def get_rows_from_table(cursor, schemas, table, column):
    cursor.execute('SELECT {} FROM {};'.format(column, '{}.'.format(schemas) + table))
    return cursor_res_to_list(cursor)


def load_from_json(conn, schemas, table):
    data = [1, [2, 3], {'a': [4, 5]}]
    my_json = json.dumps(data)
    insert_query = "insert into {} (j) values (%s) returning j".format(schemas + '.' + table)
    conn.execute(insert_query, (my_json,))
    print(conn.fetchone()[0])


def get_table_attributes(curs, schemas, table):
    curs.execute("SELECT * FROM {} LIMIT 0".format(schemas + '.' + table))
    attr = [desc[0] for desc in curs.description]
    return attr
