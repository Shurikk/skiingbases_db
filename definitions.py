import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
RESOURCES_DIR = ROOT_DIR + '\\resources\\'

WINDOW_RESOLUTION = {
    'main': {
        'x': 480,
        'y': 640},
    'login': {
        'x': 480,
        'y': 640}}
