# Skiing bases DB GUI manager
This app aims to help you in managing skiing bases database.
It also provides a great variety of useful features like:
* authorization and DB connection
* displaying DB structure diagram
* DB structure recreating
* clearing DB data
* dropping DB
* generating random data
* data insertion through special table
* displaying data
* data deletion by condition
* data statistics and plotting


Created using Python 3 with PyQt5 & Pandas.

Launch instructions from command line:
```
pip install -r ..\requirements.txt
python3 ..\src\main.py
```